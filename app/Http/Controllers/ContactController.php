<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if (Contact::all() == '') {
            return 'Sin registros';
       }else{
          return Contact::all();
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new Contact();

        // Validamos de que el usuario no haga más de 1 envío
        if ( DB::table('contacts')->where('correo', $request->input('correo'))->exists()) {
            $response = array("message"=>"This email was already registered, just must be send a message per day.", "process"=>false);
            return json_encode($response);
        }else{

            $contact->name = $request->input('name');
            $contact->apellido = $request->input('apellido');
            $contact->correo = $request->input('correo');
            $contact->celular = $request->input('celular');
            $contact->area = $request->input('area');
            $contact->mensaje = $request->input('mensaje');
            $contact->save();

            $response = array("message"=>"The message was save successful", "process"=>true);
            return json_encode($response);
        }


        // $contact->name = $request->input('name');
        // $contact->apellido = $request->input('apellido');
        // $contact->correo = $request->input('correo');
        // $contact->celular = $request->input('celular');
        // $contact->area = $request->input('area');
        // $contact->mensaje = $request->input('mensaje');
        // $contact->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $contact = new Contact();
        return Contact::findOrFail($id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
