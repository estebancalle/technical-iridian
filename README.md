
## Prueba técnica Iridian


Esta app cuenta con un back hecho en laravel y un Front 
en Vue Js


## Instalacioon DB

Para la instalación de la base de datos, en PhpMyAdmin, debemos de crear
una bd que se llame "contactform", luego de eso, importamos 
la base de datos que está en la raiz, llamada bd_proyecto.sql
## Instalación del proyecto


lo primero que debemos de hacer es instalar las dependencias de composer dentro de la raíz del proyecto
```bash
  $ composer install
```

Luego, para prender el servidor, ejecutamos

```bash
  $ php artisan server
```

Cuando ya tengamos iniciado nuestro servidor Backend
debemos de navegar a nuestro front directamente desde la consola

```bash
  $ cd/frontend
```

Cuando ya estemos ubicados sobre la raíz del fichero frontend, ejecutamos

```bash
  $ npm install
```

Luego de que se instalen las dependencias de node, podemos abrir el proyecto

```bash
  $ npm run dev
```

la consola nos dirá la ruta, que por defecto es :http://127.0.0.1:5173/

Pero puede cambiar según el puerto que tengas para cada servicio


## Autores

- [@estebancalle](https://gitlab.com/estebancalle)

